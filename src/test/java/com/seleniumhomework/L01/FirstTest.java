package com.seleniumhomework.L01;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.File;

public class FirstTest {

    WebDriver driver = null;
    String googleUrl = "https://www.google.ru/";
    String expectedTitle = "Google";
    String actualTitle = "";
    String current_env = "opera";

    @BeforeTest
    public void initBrowser() {
        if (current_env.equals("firefox")) {
            File file = new File("src/test/resources/drivers/geckodriver.exe");
            String pathToGeckoDriver = file.getAbsolutePath();
            System.setProperty("webdriver.gecko.driver", pathToGeckoDriver);
            driver = new FirefoxDriver();
            driver.manage().window().maximize();
        }
        else if (current_env.equals("chrome")){
            File file = new File("src/test/resources/drivers/chromedriver.exe");
            String pathToChromeDriver = file.getAbsolutePath();
            System.setProperty("webdriver.chrome.driver", pathToChromeDriver);
            driver = new ChromeDriver();
            driver.manage().window().maximize();
        }
        else if (current_env.equals("opera")) {
            File file = new File("src/test/resources/drivers/operadriver.exe");
            String pathToOperaDriver = file.getAbsolutePath();
            System.setProperty("webdriver.opera.driver", pathToOperaDriver);
            driver = new OperaDriver();
            driver.manage().window().maximize();
        }
        else
        {
            System.exit(0);
        }
    }

    @AfterTest
    public void closeBrowser() {
        driver.quit();
    }

}
